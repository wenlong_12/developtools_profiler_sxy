request_id: 1
 session_config {
  buffers {
   pages: 16384
  }
 }
 plugin_configs {
  plugin_name: "ftrace-plugin"
  sample_interval: 1000
  config_data {
   ftrace_events: "sched/sched_switch"
   ftrace_events: "power/suspend_resume"
   ftrace_events: "sched/sched_wakeup"
   ftrace_events: "sched/sched_wakeup_new"
   ftrace_events: "sched/sched_waking"
   ftrace_events: "sched/sched_process_exit"
   ftrace_events: "sched/sched_process_free"
   ftrace_events: "task/task_newtask"
   ftrace_events: "task/task_rename"
   ftrace_events: "power/cpu_frequency"
   ftrace_events: "power/cpu_idle"
   hitrace_categories: "ability"
   hitrace_categories: "ace"
   hitrace_categories: "app"
   hitrace_categories: "ark"
   hitrace_categories: "binder"
   hitrace_categories: "disk"
   hitrace_categories: "freq"
   hitrace_categories: "graphic"
   hitrace_categories: "idle"
   hitrace_categories: "irq"
   hitrace_categories: "memreclaim"
   hitrace_categories: "mmc"
   hitrace_categories: "multimodalinput"
   hitrace_categories: "ohos"
   hitrace_categories: "pagecache"
   hitrace_categories: "rpc"
   hitrace_categories: "sched"
   hitrace_categories: "sync"
   hitrace_categories: "window"
   hitrace_categories: "workq"
   hitrace_categories: "zaudio"
   hitrace_categories: "zcamera"
   hitrace_categories: "zimage"
   hitrace_categories: "zmedia"
   buffer_size_kb: 2048
   flush_interval_ms: 1000
   flush_threshold_kb: 4096
   parse_ksyms: true
   clock: "boot"
   trace_period_ms: 200
   debug_on: false
   hitrace_time: 30
  }
 }
 plugin_configs {
  plugin_name: "process-plugin"
  sample_interval: 1000
  config_data {
   report_process_tree: true
   report_cpu: true
   report_diskio: true
   report_pss: true
  }
 }
 plugin_configs {
  plugin_name: "cpu-plugin"
  sample_interval: 1000
  config_data {
   report_process_info: true
  }
 }
 plugin_configs {
  plugin_name: "diskio-plugin"
  sample_interval: 1000
  config_data {
   report_io_stats: IO_REPORT
  }
 }
 plugin_configs {
  plugin_name: "network-plugin"
  sample_interval: 1000
  config_data {
   test_file: "/data/local/tmp/"
  }
 }
 plugin_configs {
  plugin_name: "memory-plugin"
  sample_interval: 5000
  config_data {
   report_process_tree: true
   report_sysmem_mem_info: true
   sys_meminfo_counters: PMEM_ACTIVE
   sys_meminfo_counters: PMEM_ACTIVE_ANON
   sys_meminfo_counters: PMEM_ACTIVE_FILE
   sys_meminfo_counters: PMEM_ANON_PAGES
   sys_meminfo_counters: PMEM_BUFFERS
   sys_meminfo_counters: PMEM_CACHED
   sys_meminfo_counters: PMEM_CMA_FREE
   sys_meminfo_counters: PMEM_CMA_TOTAL
   sys_meminfo_counters: PMEM_COMMIT_LIMIT
   sys_meminfo_counters: PMEM_COMMITED_AS
   sys_meminfo_counters: PMEM_DIRTY
   sys_meminfo_counters: PMEM_INACTIVE
   sys_meminfo_counters: PMEM_INACTIVE_ANON
   sys_meminfo_counters: PMEM_INACTIVE_FILE
   sys_meminfo_counters: PMEM_KERNEL_STACK
   sys_meminfo_counters: PMEM_MAPPED
   sys_meminfo_counters: PMEM_MEM_AVAILABLE
   sys_meminfo_counters: PMEM_MEM_FREE
   sys_meminfo_counters: PMEM_MEM_TOTAL
   sys_meminfo_counters: PMEM_MLOCKED
   sys_meminfo_counters: PMEM_PAGE_TABLES
   sys_meminfo_counters: PMEM_SHMEM
   sys_meminfo_counters: PMEM_SLAB
   sys_meminfo_counters: PMEM_SLAB_RECLAIMABLE
   sys_meminfo_counters: PMEM_SLAB_UNRECLAIMABLE
   sys_meminfo_counters: PMEM_SWAP_CACHED
   sys_meminfo_counters: PMEM_SWAP_FREE
   sys_meminfo_counters: PMEM_SWAP_TOTAL
   sys_meminfo_counters: PMEM_UNEVICTABLE
   sys_meminfo_counters: PMEM_VMALLOC_CHUNK
   sys_meminfo_counters: PMEM_VMALLOC_TOTAL
   sys_meminfo_counters: PMEM_VMALLOC_USED
   sys_meminfo_counters: PMEM_WRITEBACK
   sys_meminfo_counters: PMEM_KERNEL_RECLAIMABLE
   report_sysmem_vmem_info: true
   sys_vmeminfo_counters: VMEMINFO_UNSPECIFIED
   sys_vmeminfo_counters: VMEMINFO_NR_FREE_PAGES
   sys_vmeminfo_counters: VMEMINFO_NR_ALLOC_BATCH
   sys_vmeminfo_counters: VMEMINFO_NR_INACTIVE_ANON
   sys_vmeminfo_counters: VMEMINFO_NR_ACTIVE_ANON
   sys_vmeminfo_counters: VMEMINFO_NR_INACTIVE_FILE
   sys_vmeminfo_counters: VMEMINFO_NR_ACTIVE_FILE
   sys_vmeminfo_counters: VMEMINFO_NR_UNEVICTABLE
   sys_vmeminfo_counters: VMEMINFO_NR_MLOCK
   sys_vmeminfo_counters: VMEMINFO_NR_ANON_PAGES
   sys_vmeminfo_counters: VMEMINFO_NR_MAPPED
   sys_vmeminfo_counters: VMEMINFO_NR_FILE_PAGES
   sys_vmeminfo_counters: VMEMINFO_NR_DIRTY
   sys_vmeminfo_counters: VMEMINFO_NR_WRITEBACK
   sys_vmeminfo_counters: VMEMINFO_NR_SLAB_RECLAIMABLE
   sys_vmeminfo_counters: VMEMINFO_NR_SLAB_UNRECLAIMABLE
   sys_vmeminfo_counters: VMEMINFO_NR_PAGE_TABLE_PAGES
   sys_vmeminfo_counters: VMEMINFO_NR_KERNEL_STACK
   sys_vmeminfo_counters: VMEMINFO_NR_OVERHEAD
   sys_vmeminfo_counters: VMEMINFO_NR_UNSTABLE
   sys_vmeminfo_counters: VMEMINFO_NR_BOUNCE
   sys_vmeminfo_counters: VMEMINFO_NR_VMSCAN_WRITE
   sys_vmeminfo_counters: VMEMINFO_NR_VMSCAN_IMMEDIATE_RECLAIM
   sys_vmeminfo_counters: VMEMINFO_NR_WRITEBACK_TEMP
   sys_vmeminfo_counters: VMEMINFO_NR_ISOLATED_ANON
   sys_vmeminfo_counters: VMEMINFO_NR_ISOLATED_FILE
   sys_vmeminfo_counters: VMEMINFO_NR_SHMEM
   sys_vmeminfo_counters: VMEMINFO_NR_DIRTIED
   sys_vmeminfo_counters: VMEMINFO_NR_WRITTEN
   sys_vmeminfo_counters: VMEMINFO_NR_PAGES_SCANNED
   sys_vmeminfo_counters: VMEMINFO_WORKINGSET_REFAULT
   sys_vmeminfo_counters: VMEMINFO_WORKINGSET_ACTIVATE
   sys_vmeminfo_counters: VMEMINFO_WORKINGSET_NODERECLAIM
   sys_vmeminfo_counters: VMEMINFO_NR_ANON_TRANSPARENT_HUGEPAGES
   sys_vmeminfo_counters: VMEMINFO_NR_FREE_CMA
   sys_vmeminfo_counters: VMEMINFO_NR_SWAPCACHE
   sys_vmeminfo_counters: VMEMINFO_NR_DIRTY_THRESHOLD
   sys_vmeminfo_counters: VMEMINFO_NR_DIRTY_BACKGROUND_THRESHOLD
   sys_vmeminfo_counters: VMEMINFO_PGPGIN
   sys_vmeminfo_counters: VMEMINFO_PGPGOUT
   sys_vmeminfo_counters: VMEMINFO_PGPGOUTCLEAN
   sys_vmeminfo_counters: VMEMINFO_PSWPIN
   sys_vmeminfo_counters: VMEMINFO_PSWPOUT
   sys_vmeminfo_counters: VMEMINFO_PGALLOC_DMA
   sys_vmeminfo_counters: VMEMINFO_PGALLOC_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGALLOC_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGFREE
   sys_vmeminfo_counters: VMEMINFO_PGACTIVATE
   sys_vmeminfo_counters: VMEMINFO_PGDEACTIVATE
   sys_vmeminfo_counters: VMEMINFO_PGFAULT
   sys_vmeminfo_counters: VMEMINFO_PGMAJFAULT
   sys_vmeminfo_counters: VMEMINFO_PGREFILL_DMA
   sys_vmeminfo_counters: VMEMINFO_PGREFILL_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGREFILL_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_KSWAPD_DMA
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_KSWAPD_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_KSWAPD_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_DIRECT_DMA
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_DIRECT_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_DIRECT_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_KSWAPD_DMA
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_KSWAPD_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_KSWAPD_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_DIRECT_DMA
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_DIRECT_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_DIRECT_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_DIRECT_THROTTLE
   sys_vmeminfo_counters: VMEMINFO_PGINODESTEAL
   sys_vmeminfo_counters: VMEMINFO_SLABS_SCANNED
   sys_vmeminfo_counters: VMEMINFO_KSWAPD_INODESTEAL
   sys_vmeminfo_counters: VMEMINFO_KSWAPD_LOW_WMARK_HIT_QUICKLY
   sys_vmeminfo_counters: VMEMINFO_KSWAPD_HIGH_WMARK_HIT_QUICKLY
   sys_vmeminfo_counters: VMEMINFO_PAGEOUTRUN
   sys_vmeminfo_counters: VMEMINFO_ALLOCSTALL
   sys_vmeminfo_counters: VMEMINFO_PGROTATED
   sys_vmeminfo_counters: VMEMINFO_DROP_PAGECACHE
   sys_vmeminfo_counters: VMEMINFO_DROP_SLAB
   sys_vmeminfo_counters: VMEMINFO_PGMIGRATE_SUCCESS
   sys_vmeminfo_counters: VMEMINFO_PGMIGRATE_FAIL
   sys_vmeminfo_counters: VMEMINFO_COMPACT_MIGRATE_SCANNED
   sys_vmeminfo_counters: VMEMINFO_COMPACT_FREE_SCANNED
   sys_vmeminfo_counters: VMEMINFO_COMPACT_ISOLATED
   sys_vmeminfo_counters: VMEMINFO_COMPACT_STALL
   sys_vmeminfo_counters: VMEMINFO_COMPACT_FAIL
   sys_vmeminfo_counters: VMEMINFO_COMPACT_SUCCESS
   sys_vmeminfo_counters: VMEMINFO_COMPACT_DAEMON_WAKE
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_CULLED
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_SCANNED 
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_RESCUED
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_MLOCKED
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_MUNLOCKED
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_CLEARED
   sys_vmeminfo_counters: VMEMINFO_UNEVICTABLE_PGS_STRANDED
   sys_vmeminfo_counters: VMEMINFO_NR_ZSPAGES
   sys_vmeminfo_counters: VMEMINFO_NR_ION_HEAP
   sys_vmeminfo_counters: VMEMINFO_NR_GPU_HEAP
   sys_vmeminfo_counters: VMEMINFO_ALLOCSTALL_DMA
   sys_vmeminfo_counters: VMEMINFO_ALLOCSTALL_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_ALLOCSTALL_NORMAL
   sys_vmeminfo_counters: VMEMINFO_COMPACT_DAEMON_FREE_SCANNED
   sys_vmeminfo_counters: VMEMINFO_COMPACT_DAEMON_MIGRATE_SCANNED
   sys_vmeminfo_counters: VMEMINFO_NR_FASTRPC
   sys_vmeminfo_counters: VMEMINFO_NR_INDIRECTLY_RECLAIMABLE
   sys_vmeminfo_counters: VMEMINFO_NR_ION_HEAP_POOL
   sys_vmeminfo_counters: VMEMINFO_NR_KERNEL_MISC_RECLAIMABLE
   sys_vmeminfo_counters: VMEMINFO_NR_SHADOW_CALL_STACK_BYTES
   sys_vmeminfo_counters: VMEMINFO_NR_SHMEM_HUGEPAGES
   sys_vmeminfo_counters: VMEMINFO_NR_SHMEM_PMDMAPPED
   sys_vmeminfo_counters: VMEMINFO_NR_UNRECLAIMABLE_PAGES
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_ACTIVE_ANON
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_ACTIVE_FILE
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_INACTIVE_ANON
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_INACTIVE_FILE
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_UNEVICTABLE
   sys_vmeminfo_counters: VMEMINFO_NR_ZONE_WRITE_PENDING
   sys_vmeminfo_counters: VMEMINFO_OOM_KILL 
   sys_vmeminfo_counters: VMEMINFO_PGLAZYFREE
   sys_vmeminfo_counters: VMEMINFO_PGLAZYFREED
   sys_vmeminfo_counters: VMEMINFO_PGREFILL
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_DIRECT
   sys_vmeminfo_counters: VMEMINFO_PGSCAN_KSWAPD
   sys_vmeminfo_counters: VMEMINFO_PGSKIP_DMA
   sys_vmeminfo_counters: VMEMINFO_PGSKIP_MOVABLE
   sys_vmeminfo_counters: VMEMINFO_PGSKIP_NORMAL
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_DIRECT
   sys_vmeminfo_counters: VMEMINFO_PGSTEAL_KSWAPD
   sys_vmeminfo_counters: VMEMINFO_SWAP_RA
   sys_vmeminfo_counters: VMEMINFO_SWAP_RA_HIT
   sys_vmeminfo_counters: VMEMINFO_WORKINGSET_RESTORE
   report_process_mem_info: true
   report_app_mem_info: false
   report_app_mem_by_memory_service: false
  }
 }
request_id: 1
 session_config {
  buffers {
   pages: 16384
  }
 }
 plugin_configs {
  plugin_name: "process-plugin"
  sample_interval: 1000
  config_data {
   report_process_tree: false
   report_cpu: false
   report_diskio: false
   report_pss: false
  }
 }